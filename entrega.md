# ENTREGA CONVOCATORIA JUNIO
Nombre: Yuriy Alejandro Moreno Salomón

Correo: ya.moreno.2021@alumnos.urjc.es

## Parte básica
La reproducción del video no funciona en el navegador Google Chrome.

Para el funcionamiento del programa se ha utilizado el orden puesto
en el enunciado y durante las pruebas del programa se han estado usando
los mismos valores para los argumentos:

1º. python3 signaling.py <signal_port>:

    - signal_port = 5000

2º. python3 streamer.py <file> <signal_ip> <signal_port>:

    - file = video_4.mp4
    - signal_ip = 127.0.0.1
    - signal_port = 5000

3º. python3 front.py <http_port> <signal_ip> <signal_port>:

    -http_port =  8000
    -signal_ip = 127.0.0.1
    -signal_port = 5000

## Parte adicional
* Información adicional para los ficheros:

  ~~~~
  En streamer.py se ha creado un diccionario nuevo llamado informacionFicheros que contiene el titulo correspondiente
  a cada uno de los videos enumerados del 1 al 6 (video_<número>.mp4), y a la hora de registrar los streamers al servidor 
  de señalización se envia el nombre del archivo con el título y la descripción que le corresponde en forma de diccionario en la variable 
  diccionarioMensaje y el servidor de señalización lo guardara en su directorio para despues pasar el listado de los archivos 
  y sus respectivos titulos al servidor frontal. Además, en caso de que se introduzca un video que no forme parte de los
  numerados y por lo tanto no tenga su título en informacionFicheros, se sacará el título en función del nombre del archivo
  cambiando los _ por espacios en blanco y se añadirá un video_ delante del nombre del archivo para el correcto funcionamiento
  del código en caso de que sea el primer streamer registrado.
  ~~~~

* Persistencia:

  ~~~~
  Como indica el enunciado se ha utilizado un archivo XML llamado directorio.xml donde el servidor de señalización guarda los 
  nombres de los clientes que piden la lista de videos de forma númerica empezando por el uno y la dirección ip y puerto del 
  cliente, también guarda el nombre del archivo del streamer y su dirección ip y puerto y la descripción y titulo del video.
  Para realizar esto, el servidor de señalización comprueba si existe el archivo XML, si no existe lo crea, después cuando se
  registra el streamer comprueba si ya estan guardados sus datos en el directirio, y en caso de que no, guarda su información,
  y crea la lista de videos con lo que tenga en el directorio, cuando alguien entra en la página web envia un mensaje de registro
  y pide la lista de videos y el servidor de señalización comprueba si ya tenia su información y si no guarda su nombre en orden
  númerico y su ip y puerto y le envia la lista de videos. En caso de que no se haya registrado ningun streamer, si existe el
  directorio.xml y hay información de streamers, aparecerá la lista de streamers que haya guardado en el directorio aunque no
  se pueda reproducir nada.
  ~~~~
  
## Enlace al video

  ~~~~
  https://youtu.be/RS_XIapJ4aQ
  ~~~~