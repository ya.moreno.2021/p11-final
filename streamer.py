import argparse
import asyncio
import math
import json
import cv2
import numpy
from aiortc import (
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer
from av import VideoFrame

import sys
import datetime

cliente = ""
offer_recibido = ""
answer_enviado = ""
bye_recibido = ""
remote_addr = ""
informacionFicheros = {
    "video_1.mp4": {"Titulo": "Alastor se enfado con Husk", "Descripcion": "En esta escena Husk hace referencia a que Alastor tampoco es libre, dando a entender que el también tiene un contraro con alguien y no es libre, lo que Alastor no se toma bien."},
    "video_2.mp4": {"Titulo": "Pelea entre Alastor y Adam", "Descripcion": "Alastor seguro de su poder decide enfrentarse a Adam, pero Adam demuestra la diferencia entre sus poderes y Alastor huye para salvarse."},
    "video_3.mp4": {"Titulo": "Adam y Lute ven a Charlie en el cielo", "Descripcion": "Charlie y Vaggie viajan al cielo para convencer a los ángeles de su idea de redimir a los pecadores y evitar su exterminio, razón por la que Adam no esta contento."},
    "video_4.mp4": {"Titulo": "Muerte de Adam", "Descripcion": "Después de la larga lucha de Adam contra todos, este es humillado por Lucifer y cae derrotado y mientras amenaza a todos, es apuñalado por la espalda por Niffty."},
    "video_5.mp4": {"Titulo": "Muerte de Sir Pentius", "Descripcion": "Sir Pentius se declara a cherri Bomb antes de lanzarse contra Adam, que acaba en su muerte, pero al final se ve como se redime y aparece en el cielo."},
    "video_6.mp4": {"Titulo": "More Than Anything", "Descripcion": "Lucifer desesperado quiere convencer a Charlie de que no vaya al cielo a intentar convencer a los ángles y sufra como el hizo"}
}



class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1
        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr


async def run(pc, player, recorder, role, args):
    log_message("Comienzo")

    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")  # Se activa cuando hace la conexion, Solo se hace en el servidor.
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

    global cliente

    # consume signaling
    if cliente == "":
        loop = asyncio.get_running_loop()

        ######## Video no existente #########
        if args.video_file not in informacionFicheros:

            nombre_sin_extension = args.video_file.split(".")[0]
            nombre_con_espacios = nombre_sin_extension.replace('_', ' ')
            # Añadir 'video_' delante de args.video_file
            args.video_file = 'video_' + args.video_file
            diccionarioMensaje = {args.video_file: {"Titulo": nombre_con_espacios, "Descripcion":"Video no almacenado"}}
        ######## Video no existente #########

        else:
            diccionarioMensaje = {args.video_file: informacionFicheros[args.video_file]}

        message = "REGISTER STREAMER-" + json.dumps(diccionarioMensaje)
        on_con_lost = loop.create_future()
        cliente = EchoClientProtocol(message, on_con_lost)
        global remote_addr
        remote_addr = (args.signal_ip, args.signal_port)
        await loop.create_datagram_endpoint(lambda: cliente, remote_addr=(remote_addr))

    while True:
        # obj = await signaling.receive()
        await wait_offer_recibido()
        offer = json.loads(offer_recibido)
        sdp = offer["sdp"]
        obj = RTCSessionDescription(sdp=sdp, type="offer")

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)
            if obj.type == "offer":  # Solo server
                # send answer
                add_tracks()
                await pc.setLocalDescription(await pc.createAnswer())
                global answer_enviado
                answer_enviado = json.dumps(pc.localDescription.__dict__)
                log_message('Mensaje de respuesta SDP al navegador enviado a' + str(remote_addr))
                print("Send: ", answer_enviado)
                cliente.transport.sendto(answer_enviado.encode())

                ########## Guardar el SDP local en el fichero de texto ##########
                str_sdp = pc.localDescription.sdp  # Saco el str del SDP local
                nombre_sin_extension = args.video_file.split(".")[0]
                with open(f"streamer_data_{nombre_sin_extension}.sdp", 'w') as f:
                    f.write(str_sdp)
                ########## Guardar el SDP local en el fichero de texto ##########

                ########## Guardar el SDP remoto en el fichero de texto ##########
                str_sdp = pc.remoteDescription.sdp  # Saco el str del SDP remoto
                with open(f"front_data_{nombre_sin_extension}.sdp", 'w') as f:
                    f.write(str_sdp)
                ########## Guardar el SDP remoto en el fichero de texto ##########
        log_message('Comienzo conexion WebRTC con el navegador')
        await wait_bye_recibido()


def log_message(message):
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    log_entry = f"{timestamp} {message}"
    sys.stderr.write(log_entry + "\n")

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        log_message('Mensaje REGISTRO enviado a ' + str(remote_addr))
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        if data.decode().split('"')[len(data.decode().split('"')) - 2] == "offer":
            # Accept the offer
            log_message('Mensaje de oferta SDP del navegador recibido de ' + str(addr))
            print("Received:", data.decode())
            global offer_recibido
            offer_recibido = data.decode()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def wait_offer_recibido():
    while offer_recibido == "":
        await asyncio.sleep(1)


async def wait_bye_recibido():
    while bye_recibido == "":
        await asyncio.sleep(1)


def reset_variables_globales():
    global offer_recibido
    global answer_enviado
    global bye_recibido
    offer_recibido = ""
    answer_enviado = ""
    bye_recibido = ""


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("video_file", help="Video file to stream")
    parser.add_argument("signal_ip", help="Signaling server IP address")
    parser.add_argument("signal_port", type=int, help="Signaling server port")
    args = parser.parse_args()
    pc = RTCPeerConnection()
    # create media source
    if args.video_file:
        player = MediaPlayer(args.video_file)
    else:
        player = None

    recorder = MediaBlackhole()

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc=pc,
                player=player,
                recorder=recorder,
                role="answer",
                args=args
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(pc.close())


if __name__ == "__main__":
    main()
